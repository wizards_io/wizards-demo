FROM maven:3-jdk-13 AS builder
RUN mkdir -p /artifacts /tmp/src
COPY . /tmp/src
WORKDIR /tmp/src
RUN mvn clean package --batch-mode -DskipTests
RUN cp target/crm*.jar /artifacts

FROM openjdk:13-jdk-slim
ARG BASE_DIR=/app
ARG GROUP=wizard
ARG USER=wizard
ARG UID=1001
ARG GID=1001

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JHIPSTER_SLEEP=0

RUN mkdir -p ${BASE_DIR} \
    && groupadd --gid ${GID} ${GROUP} \
    && useradd --no-user-group --shell /bin/bash --uid ${UID} --gid ${GID} --home-dir ${BASE_DIR} ${USER} \
    && chown -R ${USER}:${USER} ${BASE_DIR}
USER ${USER}
WORKDIR ${BASE_DIR}
COPY --from=builder /artifacts/crm*.jar crm.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","crm.jar"]
