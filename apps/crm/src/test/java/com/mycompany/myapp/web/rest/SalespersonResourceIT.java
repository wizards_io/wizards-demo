package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CrmApp;
import com.mycompany.myapp.domain.Salesperson;
import com.mycompany.myapp.repository.SalespersonRepository;
import com.mycompany.myapp.service.SalespersonService;
import com.mycompany.myapp.service.dto.SalespersonDTO;
import com.mycompany.myapp.service.mapper.SalespersonMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SalespersonResource} REST controller.
 */
@SpringBootTest(classes = CrmApp.class)
public class SalespersonResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PESEL = "AAAAAAAAAAA";
    private static final String UPDATED_PESEL = "BBBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final Double DEFAULT_SALARY = 1D;
    private static final Double UPDATED_SALARY = 2D;

    private static final String DEFAULT_JOB_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_JOB_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_STREET_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STREET_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_BUILDING_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_BUILDING_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_LOCAL_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_LOCAL_NUMBER = "BBBBBBBBBB";

    @Autowired
    private SalespersonRepository salespersonRepository;

    @Autowired
    private SalespersonMapper salespersonMapper;

    @Autowired
    private SalespersonService salespersonService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSalespersonMockMvc;

    private Salesperson salesperson;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SalespersonResource salespersonResource = new SalespersonResource(salespersonService);
        this.restSalespersonMockMvc = MockMvcBuilders.standaloneSetup(salespersonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Salesperson createEntity(EntityManager em) {
        Salesperson salesperson = new Salesperson()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .pesel(DEFAULT_PESEL)
            .birthDate(DEFAULT_BIRTH_DATE)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .salary(DEFAULT_SALARY)
            .jobTitle(DEFAULT_JOB_TITLE)
            .streetName(DEFAULT_STREET_NAME)
            .zipCode(DEFAULT_ZIP_CODE)
            .city(DEFAULT_CITY)
            .buildingNumber(DEFAULT_BUILDING_NUMBER)
            .localNumber(DEFAULT_LOCAL_NUMBER);
        return salesperson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Salesperson createUpdatedEntity(EntityManager em) {
        Salesperson salesperson = new Salesperson()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .pesel(UPDATED_PESEL)
            .birthDate(UPDATED_BIRTH_DATE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .salary(UPDATED_SALARY)
            .jobTitle(UPDATED_JOB_TITLE)
            .streetName(UPDATED_STREET_NAME)
            .zipCode(UPDATED_ZIP_CODE)
            .city(UPDATED_CITY)
            .buildingNumber(UPDATED_BUILDING_NUMBER)
            .localNumber(UPDATED_LOCAL_NUMBER);
        return salesperson;
    }

    @BeforeEach
    public void initTest() {
        salesperson = createEntity(em);
    }

    @Test
    @Transactional
    public void createSalesperson() throws Exception {
        int databaseSizeBeforeCreate = salespersonRepository.findAll().size();

        // Create the Salesperson
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(salesperson);
        restSalespersonMockMvc.perform(post("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isCreated());

        // Validate the Salesperson in the database
        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeCreate + 1);
        Salesperson testSalesperson = salespersonList.get(salespersonList.size() - 1);
        assertThat(testSalesperson.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testSalesperson.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testSalesperson.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testSalesperson.getPesel()).isEqualTo(DEFAULT_PESEL);
        assertThat(testSalesperson.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
        assertThat(testSalesperson.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testSalesperson.getSalary()).isEqualTo(DEFAULT_SALARY);
        assertThat(testSalesperson.getJobTitle()).isEqualTo(DEFAULT_JOB_TITLE);
        assertThat(testSalesperson.getStreetName()).isEqualTo(DEFAULT_STREET_NAME);
        assertThat(testSalesperson.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testSalesperson.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testSalesperson.getBuildingNumber()).isEqualTo(DEFAULT_BUILDING_NUMBER);
        assertThat(testSalesperson.getLocalNumber()).isEqualTo(DEFAULT_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void createSalespersonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = salespersonRepository.findAll().size();

        // Create the Salesperson with an existing ID
        salesperson.setId(1L);
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(salesperson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSalespersonMockMvc.perform(post("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Salesperson in the database
        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = salespersonRepository.findAll().size();
        // set the field null
        salesperson.setFirstName(null);

        // Create the Salesperson, which fails.
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(salesperson);

        restSalespersonMockMvc.perform(post("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isBadRequest());

        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = salespersonRepository.findAll().size();
        // set the field null
        salesperson.setLastName(null);

        // Create the Salesperson, which fails.
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(salesperson);

        restSalespersonMockMvc.perform(post("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isBadRequest());

        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSalespeople() throws Exception {
        // Initialize the database
        salespersonRepository.saveAndFlush(salesperson);

        // Get all the salespersonList
        restSalespersonMockMvc.perform(get("/api/salespeople?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(salesperson.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].pesel").value(hasItem(DEFAULT_PESEL)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].salary").value(hasItem(DEFAULT_SALARY.doubleValue())))
            .andExpect(jsonPath("$.[*].jobTitle").value(hasItem(DEFAULT_JOB_TITLE)))
            .andExpect(jsonPath("$.[*].streetName").value(hasItem(DEFAULT_STREET_NAME)))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].buildingNumber").value(hasItem(DEFAULT_BUILDING_NUMBER)))
            .andExpect(jsonPath("$.[*].localNumber").value(hasItem(DEFAULT_LOCAL_NUMBER)));
    }
    
    @Test
    @Transactional
    public void getSalesperson() throws Exception {
        // Initialize the database
        salespersonRepository.saveAndFlush(salesperson);

        // Get the salesperson
        restSalespersonMockMvc.perform(get("/api/salespeople/{id}", salesperson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(salesperson.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.pesel").value(DEFAULT_PESEL))
            .andExpect(jsonPath("$.birthDate").value(DEFAULT_BIRTH_DATE.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.salary").value(DEFAULT_SALARY.doubleValue()))
            .andExpect(jsonPath("$.jobTitle").value(DEFAULT_JOB_TITLE))
            .andExpect(jsonPath("$.streetName").value(DEFAULT_STREET_NAME))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.buildingNumber").value(DEFAULT_BUILDING_NUMBER))
            .andExpect(jsonPath("$.localNumber").value(DEFAULT_LOCAL_NUMBER));
    }

    @Test
    @Transactional
    public void getNonExistingSalesperson() throws Exception {
        // Get the salesperson
        restSalespersonMockMvc.perform(get("/api/salespeople/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSalesperson() throws Exception {
        // Initialize the database
        salespersonRepository.saveAndFlush(salesperson);

        int databaseSizeBeforeUpdate = salespersonRepository.findAll().size();

        // Update the salesperson
        Salesperson updatedSalesperson = salespersonRepository.findById(salesperson.getId()).get();
        // Disconnect from session so that the updates on updatedSalesperson are not directly saved in db
        em.detach(updatedSalesperson);
        updatedSalesperson
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .pesel(UPDATED_PESEL)
            .birthDate(UPDATED_BIRTH_DATE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .salary(UPDATED_SALARY)
            .jobTitle(UPDATED_JOB_TITLE)
            .streetName(UPDATED_STREET_NAME)
            .zipCode(UPDATED_ZIP_CODE)
            .city(UPDATED_CITY)
            .buildingNumber(UPDATED_BUILDING_NUMBER)
            .localNumber(UPDATED_LOCAL_NUMBER);
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(updatedSalesperson);

        restSalespersonMockMvc.perform(put("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isOk());

        // Validate the Salesperson in the database
        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeUpdate);
        Salesperson testSalesperson = salespersonList.get(salespersonList.size() - 1);
        assertThat(testSalesperson.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testSalesperson.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testSalesperson.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testSalesperson.getPesel()).isEqualTo(UPDATED_PESEL);
        assertThat(testSalesperson.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
        assertThat(testSalesperson.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testSalesperson.getSalary()).isEqualTo(UPDATED_SALARY);
        assertThat(testSalesperson.getJobTitle()).isEqualTo(UPDATED_JOB_TITLE);
        assertThat(testSalesperson.getStreetName()).isEqualTo(UPDATED_STREET_NAME);
        assertThat(testSalesperson.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testSalesperson.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testSalesperson.getBuildingNumber()).isEqualTo(UPDATED_BUILDING_NUMBER);
        assertThat(testSalesperson.getLocalNumber()).isEqualTo(UPDATED_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void updateNonExistingSalesperson() throws Exception {
        int databaseSizeBeforeUpdate = salespersonRepository.findAll().size();

        // Create the Salesperson
        SalespersonDTO salespersonDTO = salespersonMapper.toDto(salesperson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSalespersonMockMvc.perform(put("/api/salespeople")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(salespersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Salesperson in the database
        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSalesperson() throws Exception {
        // Initialize the database
        salespersonRepository.saveAndFlush(salesperson);

        int databaseSizeBeforeDelete = salespersonRepository.findAll().size();

        // Delete the salesperson
        restSalespersonMockMvc.perform(delete("/api/salespeople/{id}", salesperson.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Salesperson> salespersonList = salespersonRepository.findAll();
        assertThat(salespersonList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Salesperson.class);
        Salesperson salesperson1 = new Salesperson();
        salesperson1.setId(1L);
        Salesperson salesperson2 = new Salesperson();
        salesperson2.setId(salesperson1.getId());
        assertThat(salesperson1).isEqualTo(salesperson2);
        salesperson2.setId(2L);
        assertThat(salesperson1).isNotEqualTo(salesperson2);
        salesperson1.setId(null);
        assertThat(salesperson1).isNotEqualTo(salesperson2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SalespersonDTO.class);
        SalespersonDTO salespersonDTO1 = new SalespersonDTO();
        salespersonDTO1.setId(1L);
        SalespersonDTO salespersonDTO2 = new SalespersonDTO();
        assertThat(salespersonDTO1).isNotEqualTo(salespersonDTO2);
        salespersonDTO2.setId(salespersonDTO1.getId());
        assertThat(salespersonDTO1).isEqualTo(salespersonDTO2);
        salespersonDTO2.setId(2L);
        assertThat(salespersonDTO1).isNotEqualTo(salespersonDTO2);
        salespersonDTO1.setId(null);
        assertThat(salespersonDTO1).isNotEqualTo(salespersonDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(salespersonMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(salespersonMapper.fromId(null)).isNull();
    }
}
