import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrmTestModule } from '../../../test.module';
import { SalespersonDetailComponent } from 'app/entities/salesperson/salesperson-detail.component';
import { Salesperson } from 'app/shared/model/salesperson.model';

describe('Component Tests', () => {
  describe('Salesperson Management Detail Component', () => {
    let comp: SalespersonDetailComponent;
    let fixture: ComponentFixture<SalespersonDetailComponent>;
    const route = ({ data: of({ salesperson: new Salesperson(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrmTestModule],
        declarations: [SalespersonDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SalespersonDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SalespersonDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.salesperson).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
