import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CrmTestModule } from '../../../test.module';
import { SalespersonDeleteDialogComponent } from 'app/entities/salesperson/salesperson-delete-dialog.component';
import { SalespersonService } from 'app/entities/salesperson/salesperson.service';

describe('Component Tests', () => {
  describe('Salesperson Management Delete Component', () => {
    let comp: SalespersonDeleteDialogComponent;
    let fixture: ComponentFixture<SalespersonDeleteDialogComponent>;
    let service: SalespersonService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrmTestModule],
        declarations: [SalespersonDeleteDialogComponent]
      })
        .overrideTemplate(SalespersonDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SalespersonDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SalespersonService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
