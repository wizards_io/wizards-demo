import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { CrmTestModule } from '../../../test.module';
import { SalespersonUpdateComponent } from 'app/entities/salesperson/salesperson-update.component';
import { SalespersonService } from 'app/entities/salesperson/salesperson.service';
import { Salesperson } from 'app/shared/model/salesperson.model';

describe('Component Tests', () => {
  describe('Salesperson Management Update Component', () => {
    let comp: SalespersonUpdateComponent;
    let fixture: ComponentFixture<SalespersonUpdateComponent>;
    let service: SalespersonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrmTestModule],
        declarations: [SalespersonUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SalespersonUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SalespersonUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SalespersonService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Salesperson(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Salesperson();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
