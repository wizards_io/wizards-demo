package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.SalespersonService;
import com.mycompany.myapp.service.dto.SalespersonDTO;
import com.mycompany.myapp.service.dto.SalespersonFindRequest;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Salesperson}.
 */
@RestController
@RequestMapping("/api")
public class SalespersonResource {

    private final Logger log = LoggerFactory.getLogger(SalespersonResource.class);

    private static final String ENTITY_NAME = "salesperson";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SalespersonService salespersonService;

    public SalespersonResource(SalespersonService salespersonService) {
        this.salespersonService = salespersonService;
    }

    /**
     * {@code POST  /salespeople} : Create a new salesperson.
     *
     * @param salespersonDTO the salespersonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new salespersonDTO, or with status {@code 400 (Bad Request)} if the salesperson has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/salespeople")
    public ResponseEntity<SalespersonDTO> createSalesperson(@Valid @RequestBody SalespersonDTO salespersonDTO) throws URISyntaxException {
        log.debug("REST request to save Salesperson : {}", salespersonDTO);
        if (salespersonDTO.getId() != null) {
            throw new BadRequestAlertException("A new salesperson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        salespersonDTO.setCreateDate(LocalDateTime.now());
        SalespersonDTO result = salespersonService.save(salespersonDTO);
        return ResponseEntity.created(new URI("/api/salespeople/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /salespeople} : Updates an existing salesperson.
     *
     * @param salespersonDTO the salespersonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated salespersonDTO,
     * or with status {@code 400 (Bad Request)} if the salespersonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the salespersonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/salespeople")
    public ResponseEntity<SalespersonDTO> updateSalesperson(@Valid @RequestBody SalespersonDTO salespersonDTO) throws URISyntaxException {
        log.debug("REST request to update Salesperson : {}", salespersonDTO);
        if (salespersonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        salespersonDTO.setUpdateDate(LocalDateTime.now());
        SalespersonDTO result = salespersonService.save(salespersonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, salespersonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /salespeople} : get all the salespeople.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of salespeople in body.
     */
    @GetMapping("/salespeople")
    public ResponseEntity<List<SalespersonDTO>> getAllSalespeople(Pageable pageable) {
        log.debug("REST request to get a page of Salespeople");
        Page<SalespersonDTO> page = salespersonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/salespeople/find")
    public ResponseEntity<List<SalespersonDTO>> findEmployees(Pageable pageable, @RequestBody SalespersonFindRequest criteria) {
        log.debug("REST request to get a page of Salespeople by criteria");
        Page<SalespersonDTO> page = salespersonService.findByCriteria(pageable, criteria);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /salespeople/:id} : get the "id" salesperson.
     *
     * @param id the id of the salespersonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the salespersonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/salespeople/{id}")
    public ResponseEntity<SalespersonDTO> getSalesperson(@PathVariable Long id) {
        log.debug("REST request to get Salesperson : {}", id);
        Optional<SalespersonDTO> salespersonDTO = salespersonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(salespersonDTO);
    }

    /**
     * {@code DELETE  /salespeople/:id} : delete the "id" salesperson.
     *
     * @param id the id of the salespersonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/salespeople/{id}")
    public ResponseEntity<Void> deleteSalesperson(@PathVariable Long id) {
        log.debug("REST request to delete Salesperson : {}", id);
        salespersonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
