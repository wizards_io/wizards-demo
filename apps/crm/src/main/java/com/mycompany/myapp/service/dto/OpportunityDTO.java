package com.mycompany.myapp.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Opportunity} entity.
 */
public class OpportunityDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String stage;

    @Min(value = 0)
    @Max(value = 100)
    private Integer probability;

    private String description;

    private LocalDate created;

    private LocalDate closeDate;


    private Long customerId;

    private String customerName;

    private Set<SalespersonDTO> salespeople = new HashSet<>();

    private Set<ContactDTO> contacts = new HashSet<>();

    private Set<ProductDTO> products = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Set<SalespersonDTO> getSalespeople() {
        return salespeople;
    }

    public void setSalespeople(Set<SalespersonDTO> salespeople) {
        this.salespeople = salespeople;
    }

    public Set<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(Set<ContactDTO> contacts) {
        this.contacts = contacts;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OpportunityDTO opportunityDTO = (OpportunityDTO) o;
        if (opportunityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), opportunityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OpportunityDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stage='" + getStage() + "'" +
            ", probability=" + getProbability() +
            ", code='" + getDescription() + "'" +
            ", created='" + getCreated() + "'" +
            ", closeDate='" + getCloseDate() + "'" +
            ", customer=" + getCustomerId() +
            ", customer='" + getCustomerName() + "'" +
            "}";
    }
}
