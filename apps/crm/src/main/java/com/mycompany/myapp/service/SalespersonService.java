package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Salesperson;
import com.mycompany.myapp.repository.SalespersonRepository;
import com.mycompany.myapp.service.dto.SalespersonDTO;
import com.mycompany.myapp.service.mapper.SalespersonMapper;
import com.mycompany.myapp.service.dto.SalespersonFindRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Salesperson}.
 */
@Service
@Transactional
public class SalespersonService {

    private final Logger log = LoggerFactory.getLogger(SalespersonService.class);

    private final SalespersonRepository salespersonRepository;

    private final SalespersonMapper salespersonMapper;

    private final SalespersonSpecification salespersonSpecification;

    public SalespersonService(SalespersonRepository salespersonRepository, SalespersonMapper salespersonMapper, SalespersonSpecification salespersonSpecification) {
        this.salespersonRepository = salespersonRepository;
        this.salespersonMapper = salespersonMapper;
        this.salespersonSpecification = salespersonSpecification;
    }

    /**
     * Save a salesperson.
     *
     * @param salespersonDTO the entity to save.
     * @return the persisted entity.
     */
    public SalespersonDTO save(SalespersonDTO salespersonDTO) {
        log.debug("Request to save Salesperson : {}", salespersonDTO);
        Salesperson salesperson = salespersonMapper.toEntity(salespersonDTO);
        salesperson = salespersonRepository.save(salesperson);
        return salespersonMapper.toDto(salesperson);
    }

    /**
     * Get all the salespeople.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SalespersonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Salespeople");
        return salespersonRepository.findAll(pageable)
            .map(salespersonMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<SalespersonDTO> findByCriteria(Pageable pageable, SalespersonFindRequest criteria) {
        log.debug("Request to get Salespeople by criteria");

        Specification<Salesperson> spec = salespersonSpecification.createSpecification(criteria);
        Page<Salesperson> page = salespersonRepository.findAll(spec, pageable);

        return page
            .map(salespersonMapper::toDto);
    }


    /**
     * Get one salesperson by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SalespersonDTO> findOne(Long id) {
        log.debug("Request to get Salesperson : {}", id);
        return salespersonRepository.findById(id)
            .map(salespersonMapper::toDto);
    }

    /**
     * Delete the salesperson by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Salesperson : {}", id);
        salespersonRepository.deleteById(id);
    }
}
