package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.OpportunityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Opportunity} and its DTO {@link OpportunityDTO}.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class, SalespersonMapper.class, ContactMapper.class, ProductMapper.class})
public interface OpportunityMapper extends EntityMapper<OpportunityDTO, Opportunity> {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customer.name", target = "customerName")
    OpportunityDTO toDto(Opportunity opportunity);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(target = "removeSalesperson", ignore = true)
    @Mapping(target = "removeContact", ignore = true)
    @Mapping(target = "removeProduct", ignore = true)
    Opportunity toEntity(OpportunityDTO opportunityDTO);

    default Opportunity fromId(Long id) {
        if (id == null) {
            return null;
        }
        Opportunity opportunity = new Opportunity();
        opportunity.setId(id);
        return opportunity;
    }
}
