package com.mycompany.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Opportunity.
 */
@Entity
@Table(name = "opportunity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Opportunity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "stage")
    private String stage;

    @Min(value = 0)
    @Max(value = 100)
    @Column(name = "probability")
    private Integer probability;

    @Column(name = "description")
    private String description;

    @Column(name = "created")
    private LocalDate created;

    @Column(name = "close_date")
    private LocalDate closeDate;

    @ManyToOne
    @JsonIgnoreProperties("opportunities")
    private Customer customer;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "opportunity_salesperson",
               joinColumns = @JoinColumn(name = "opportunity_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "salesperson_id", referencedColumnName = "id"))
    private Set<Salesperson> salespeople = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "opportunity_contact",
               joinColumns = @JoinColumn(name = "opportunity_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"))
    private Set<Contact> contacts = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "opportunity_product",
               joinColumns = @JoinColumn(name = "opportunity_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
    private Set<Product> products = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Opportunity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStage() {
        return stage;
    }

    public Opportunity stage(String stage) {
        this.stage = stage;
        return this;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Integer getProbability() {
        return probability;
    }

    public Opportunity probability(Integer probability) {
        this.probability = probability;
        return this;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public String getDescription() {
        return description;
    }

    public Opportunity description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreated() {
        return created;
    }

    public Opportunity created(LocalDate created) {
        this.created = created;
        return this;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public Opportunity closeDate(LocalDate closeDate) {
        this.closeDate = closeDate;
        return this;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Opportunity customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Salesperson> getSalespeople() {
        return salespeople;
    }

    public Opportunity salespeople(Set<Salesperson> salespeople) {
        this.salespeople = salespeople;
        return this;
    }

    public Opportunity addSalesperson(Salesperson salesperson) {
        this.salespeople.add(salesperson);
        salesperson.getOpportunities().add(this);
        return this;
    }

    public Opportunity removeSalesperson(Salesperson salesperson) {
        this.salespeople.remove(salesperson);
        salesperson.getOpportunities().remove(this);
        return this;
    }

    public void setSalespeople(Set<Salesperson> salespeople) {
        this.salespeople = salespeople;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public Opportunity contacts(Set<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Opportunity addContact(Contact contact) {
        this.contacts.add(contact);
        contact.getOpportunities().add(this);
        return this;
    }

    public Opportunity removeContact(Contact contact) {
        this.contacts.remove(contact);
        contact.getOpportunities().remove(this);
        return this;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public Opportunity products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public Opportunity addProduct(Product product) {
        this.products.add(product);
        product.getOpportunities().add(this);
        return this;
    }

    public Opportunity removeProduct(Product product) {
        this.products.remove(product);
        product.getOpportunities().remove(this);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Opportunity)) {
            return false;
        }
        return id != null && id.equals(((Opportunity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Opportunity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stage='" + getStage() + "'" +
            ", probability=" + getProbability() +
            ", description='" + getDescription() + "'" +
            ", created='" + getCreated() + "'" +
            ", closeDate='" + getCloseDate() + "'" +
            "}";
    }
}
