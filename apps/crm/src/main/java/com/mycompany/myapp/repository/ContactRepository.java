package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.Contact;
import com.mycompany.myapp.domain.Salesperson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Contact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    Page<Contact> findAll(Specification<Contact> spec, Pageable pageable);
}
