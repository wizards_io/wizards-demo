package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.domain.Customer_;
import com.mycompany.myapp.service.dto.CustomerFindRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;

@Service
public class CustomerSpecification {
    public Specification<Customer> createSpecification(CustomerFindRequest criteria) {
        return (root, cq, cb) -> {
            Predicate pr = cb.isNotNull(root.get(Customer_.NAME));

            if (criteria.getId() != null) {
                try {
                    Long requestedId = Long.parseLong(criteria.getId());
                    pr = cb.and(pr, cb.equal(root.get(Customer_.ID), requestedId));
                } catch (NumberFormatException e) {
                    pr = cb.isTrue(cb.isNull(root.get(Customer_.ID)));
                }
            }
            if (criteria.getName() != null && !criteria.getName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Customer_.NAME)), criteria.getName().toLowerCase()));
            }
            if (criteria.getNip() != null && !criteria.getNip().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Customer_.NIP)), criteria.getNip().toLowerCase()));
            }
            if (criteria.getRegon() != null && !criteria.getRegon().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Customer_.REGON)), criteria.getRegon().toLowerCase()));
            }
            return pr;
        };
    }
}
