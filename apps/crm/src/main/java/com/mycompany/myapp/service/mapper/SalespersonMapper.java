package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.SalespersonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Salesperson} and its DTO {@link SalespersonDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SalespersonMapper extends EntityMapper<SalespersonDTO, Salesperson> {


    @Mapping(target = "opportunities", ignore = true)
    @Mapping(target = "removeOpportunity", ignore = true)
    Salesperson toEntity(SalespersonDTO salespersonDTO);

    default Salesperson fromId(Long id) {
        if (id == null) {
            return null;
        }
        Salesperson salesperson = new Salesperson();
        salesperson.setId(id);
        return salesperson;
    }
}
