package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Contact;
import com.mycompany.myapp.domain.Contact_;
import com.mycompany.myapp.domain.Customer_;
import com.mycompany.myapp.service.dto.ContactFindRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;

@Service
public class ContactSpecification {
    public Specification<Contact> createSpecification(ContactFindRequest criteria) {
        return (root, cq, cb) -> {
            Predicate pr = cb.isNotNull(root.get(Contact_.FIRST_NAME));

            if (criteria.getId() != null) {
                try {
                    Long requestedId = Long.parseLong(criteria.getId());
                    pr = cb.and(pr, cb.equal(root.get(Contact_.ID), requestedId));
                } catch (NumberFormatException e) {
                    pr = cb.isTrue(cb.isNull(root.get(Contact_.ID)));
                }
            }
            if (criteria.getFirstName() != null && !criteria.getFirstName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Contact_.FIRST_NAME)), criteria.getFirstName().toLowerCase()));
            }
            if (criteria.getLastName() != null && !criteria.getLastName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Contact_.LAST_NAME)), criteria.getLastName().toLowerCase()));
            }
            if (criteria.getEmail() != null && !criteria.getEmail().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Contact_.EMAIL)), criteria.getEmail().toLowerCase()));
            }
            return pr;
        };
    }
}
