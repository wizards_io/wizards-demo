package com.mycompany.myapp.repository;
import com.mycompany.myapp.domain.Opportunity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Opportunity entity.
 */
@Repository
public interface OpportunityRepository extends JpaRepository<Opportunity, Long> {

    @Query(value = "select distinct opportunity from Opportunity opportunity left join fetch opportunity.salespeople left join fetch opportunity.contacts left join fetch opportunity.products",
        countQuery = "select count(distinct opportunity) from Opportunity opportunity")
    Page<Opportunity> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct opportunity from Opportunity opportunity left join fetch opportunity.salespeople left join fetch opportunity.contacts left join fetch opportunity.products")
    List<Opportunity> findAllWithEagerRelationships();

    @Query("select opportunity from Opportunity opportunity left join fetch opportunity.salespeople left join fetch opportunity.contacts left join fetch opportunity.products where opportunity.id =:id")
    Optional<Opportunity> findOneWithEagerRelationships(@Param("id") Long id);

}
