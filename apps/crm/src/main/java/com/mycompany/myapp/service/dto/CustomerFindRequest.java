package com.mycompany.myapp.service.dto;

public class CustomerFindRequest {
    private String id;
    private String name;
    private String nip;
    private String regon;

    public CustomerFindRequest() {}

    public CustomerFindRequest(String id, String name, String nip, String regon) {
        this.id = id;
        this.name = name;
        this.nip = nip;
        this.regon = regon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }
}
