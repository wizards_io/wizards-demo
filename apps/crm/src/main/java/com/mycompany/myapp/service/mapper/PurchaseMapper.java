package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Purchase;
import com.mycompany.myapp.service.dto.PurchaseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface PurchaseMapper extends EntityMapper<PurchaseDTO, Purchase> {

    Purchase toEntity(PurchaseDTO purchaseDTO);

    default Purchase fromId(Long id) {
        if (id == null) {
            return null;
        }
        Purchase purchase = new Purchase();
        purchase.setId(id);
        return purchase;
    }
}
