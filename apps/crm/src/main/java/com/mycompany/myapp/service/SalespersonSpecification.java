package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Salesperson;
import com.mycompany.myapp.domain.Salesperson_;
import com.mycompany.myapp.service.dto.SalespersonFindRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;

@Service
public class SalespersonSpecification {
    public Specification<Salesperson> createSpecification(SalespersonFindRequest criteria) {
        return (root, cq, cb) -> {
            Predicate pr = cb.isNotNull(root.get(Salesperson_.FIRST_NAME));

            if (criteria.getId() != null) {
                try {
                    Long requestedId = Long.parseLong(criteria.getId());
                    pr = cb.and(pr, cb.equal(root.get(Salesperson_.ID), requestedId));
                } catch (NumberFormatException e) {
                    pr = cb.isTrue(cb.isNull(root.get(Salesperson_.ID)));
                }
            }
            if (criteria.getFirstName() != null && !criteria.getFirstName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Salesperson_.FIRST_NAME)), criteria.getFirstName().toLowerCase()));
            }
            if (criteria.getLastName() != null && !criteria.getLastName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Salesperson_.LAST_NAME)), criteria.getLastName().toLowerCase()));
            }
            if (criteria.getEmail() != null && !criteria.getEmail().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Salesperson_.EMAIL)), criteria.getEmail().toLowerCase()));
            }
            return pr;
        };
    }
}
