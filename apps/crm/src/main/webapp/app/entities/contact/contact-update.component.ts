import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IContact, Contact } from 'app/shared/model/contact.model';
import { ContactService } from './contact.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';
import { IOpportunity } from 'app/shared/model/opportunity.model';
import { OpportunityService } from 'app/entities/opportunity/opportunity.service';

@Component({
  selector: 'jhi-contact-update',
  templateUrl: './contact-update.component.html'
})
export class ContactUpdateComponent implements OnInit {
  isSaving: boolean;

  customers: ICustomer[];

  opportunities: IOpportunity[];

  editForm = this.fb.group({
    id: [],
    firstName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    lastName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    email: [],
    phoneNumber: [null, [Validators.maxLength(20)]],
    jobTitle: [],
    streetName: [],
    zipCode: [],
    city: [],
    buildingNumber: [],
    localNumber: [],
    customerId: [],
    createDate: [],
    updateDate: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected contactService: ContactService,
    protected customerService: CustomerService,
    protected opportunityService: OpportunityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ contact }) => {
      this.updateForm(contact);
    });
    this.customerService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICustomer[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICustomer[]>) => response.body)
      )
      .subscribe((res: ICustomer[]) => (this.customers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.opportunityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOpportunity[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOpportunity[]>) => response.body)
      )
      .subscribe((res: IOpportunity[]) => (this.opportunities = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(contact: IContact) {
    this.editForm.patchValue({
      id: contact.id,
      firstName: contact.firstName,
      lastName: contact.lastName,
      email: contact.email,
      phoneNumber: contact.phoneNumber,
      streetName: contact.streetName,
      zipCode: contact.zipCode,
      city: contact.city,
      buildingNumber: contact.buildingNumber,
      localNumber: contact.localNumber,
      customerId: contact.customerId,
      createDate: contact.createDate,
      updateDate: contact.updateDate
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const contact = this.createFromForm();
    if (contact.id !== undefined) {
      this.subscribeToSaveResponse(this.contactService.update(contact));
    } else {
      this.subscribeToSaveResponse(this.contactService.create(contact));
    }
  }

  private createFromForm(): IContact {
    return {
      ...new Contact(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      email: this.editForm.get(['email']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      streetName: this.editForm.get(['streetName']).value,
      zipCode: this.editForm.get(['zipCode']).value,
      city: this.editForm.get(['city']).value,
      buildingNumber: this.editForm.get(['buildingNumber']).value,
      localNumber: this.editForm.get(['localNumber']).value,
      customerId: this.editForm.get(['customerId']).value,
      createDate: this.editForm.get(['createDate']).value,
      updateDate: this.editForm.get(['updateDate']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContact>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCustomerById(index: number, item: ICustomer) {
    return item.id;
  }

  trackOpportunityById(index: number, item: IOpportunity) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
