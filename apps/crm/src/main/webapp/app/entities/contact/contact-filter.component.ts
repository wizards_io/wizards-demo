import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ContactCriteria } from 'app/entities/contact/contact-criteria';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

@Component({
  selector: 'jhi-contact-filter',
  templateUrl: './contact-filter.component.html'
})
export class ContactFilterComponent {
  @Input() request: ContactCriteria;
  @Output() onFind: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  find() {
    this.onFind.emit(this.request);
  }

  clear() {
    this.request.id = null;
    this.request.firstName = null;
    this.request.lastName = null;
    this.request.email = null;
    this.onFind.emit();
  }
}
