import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrmSharedModule } from 'app/shared/shared.module';
import { ContactComponent } from './contact.component';
import { ContactDetailComponent } from './contact-detail.component';
import { ContactUpdateComponent } from './contact-update.component';
import { ContactDeletePopupComponent, ContactDeleteDialogComponent } from './contact-delete-dialog.component';
import { contactRoute, contactPopupRoute } from './contact.route';
import { ContactFilterComponent } from 'app/entities/contact/contact-filter.component';

const ENTITY_STATES = [...contactRoute, ...contactPopupRoute];

@NgModule({
  imports: [CrmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ContactComponent,
    ContactDetailComponent,
    ContactUpdateComponent,
    ContactDeleteDialogComponent,
    ContactDeletePopupComponent,
    ContactFilterComponent
  ],
  entryComponents: [ContactDeleteDialogComponent]
})
export class CrmContactModule {}
