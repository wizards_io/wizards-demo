export class SalespersonCriteria {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
}
