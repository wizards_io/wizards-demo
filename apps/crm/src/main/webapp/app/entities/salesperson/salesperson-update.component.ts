import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISalesperson, Salesperson } from 'app/shared/model/salesperson.model';
import { SalespersonService } from './salesperson.service';
import { IOpportunity } from 'app/shared/model/opportunity.model';
import { OpportunityService } from 'app/entities/opportunity/opportunity.service';

@Component({
  selector: 'jhi-salesperson-update',
  templateUrl: './salesperson-update.component.html'
})
export class SalespersonUpdateComponent implements OnInit {
  isSaving: boolean;

  opportunities: IOpportunity[];
  birthDateDp: any;

  editForm = this.fb.group({
    id: [],
    firstName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    lastName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    email: [null, [Validators.maxLength(60)]],
    pesel: [null, [Validators.minLength(11), Validators.maxLength(11)]],
    birthDate: [],
    phoneNumber: [null, [Validators.maxLength(20)]],
    salary: [],
    jobTitle: [],
    streetName: [],
    zipCode: [],
    city: [],
    buildingNumber: [],
    localNumber: [],
    createDate: [],
    updateDate: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected salespersonService: SalespersonService,
    protected opportunityService: OpportunityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ salesperson }) => {
      this.updateForm(salesperson);
    });
    this.opportunityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOpportunity[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOpportunity[]>) => response.body)
      )
      .subscribe((res: IOpportunity[]) => (this.opportunities = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(salesperson: ISalesperson) {
    this.editForm.patchValue({
      id: salesperson.id,
      firstName: salesperson.firstName,
      lastName: salesperson.lastName,
      email: salesperson.email,
      pesel: salesperson.pesel,
      birthDate: salesperson.birthDate,
      phoneNumber: salesperson.phoneNumber,
      salary: salesperson.salary,
      jobTitle: salesperson.jobTitle,
      streetName: salesperson.streetName,
      zipCode: salesperson.zipCode,
      city: salesperson.city,
      buildingNumber: salesperson.buildingNumber,
      localNumber: salesperson.localNumber,
      createDate: salesperson.createDate,
      updateDate: salesperson.updateDate
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const salesperson = this.createFromForm();
    if (salesperson.id !== undefined) {
      this.subscribeToSaveResponse(this.salespersonService.update(salesperson));
    } else {
      this.subscribeToSaveResponse(this.salespersonService.create(salesperson));
    }
  }

  private createFromForm(): ISalesperson {
    return {
      ...new Salesperson(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      email: this.editForm.get(['email']).value,
      pesel: this.editForm.get(['pesel']).value,
      birthDate: this.editForm.get(['birthDate']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      salary: this.editForm.get(['salary']).value,
      streetName: this.editForm.get(['streetName']).value,
      zipCode: this.editForm.get(['zipCode']).value,
      city: this.editForm.get(['city']).value,
      buildingNumber: this.editForm.get(['buildingNumber']).value,
      localNumber: this.editForm.get(['localNumber']).value,
      createDate: this.editForm.get(['createDate']).value,
      updateDate: this.editForm.get(['updateDate']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISalesperson>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOpportunityById(index: number, item: IOpportunity) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
