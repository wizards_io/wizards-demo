import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SalespersonCriteria } from 'app/entities/salesperson/salesperson-criteria';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

@Component({
  selector: 'jhi-salesperson-filter',
  templateUrl: './salesperson-filter.component.html'
})
export class SalespersonFilterComponent {
  @Input() request: SalespersonCriteria;
  @Output() onFind: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  find() {
    this.onFind.emit(this.request);
  }

  clear() {
    this.request.id = null;
    this.request.firstName = null;
    this.request.lastName = null;
    this.request.email = null;
    this.onFind.emit();
  }
}
