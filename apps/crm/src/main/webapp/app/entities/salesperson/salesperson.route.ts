import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Salesperson } from 'app/shared/model/salesperson.model';
import { SalespersonService } from './salesperson.service';
import { SalespersonComponent } from './salesperson.component';
import { SalespersonDetailComponent } from './salesperson-detail.component';
import { SalespersonUpdateComponent } from './salesperson-update.component';
import { SalespersonDeletePopupComponent } from './salesperson-delete-dialog.component';
import { ISalesperson } from 'app/shared/model/salesperson.model';

@Injectable({ providedIn: 'root' })
export class SalespersonResolve implements Resolve<ISalesperson> {
  constructor(private service: SalespersonService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISalesperson> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Salesperson>) => response.ok),
        map((salesperson: HttpResponse<Salesperson>) => salesperson.body)
      );
    }
    return of(new Salesperson());
  }
}

export const salespersonRoute: Routes = [
  {
    path: '',
    component: SalespersonComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'crmApp.salesperson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SalespersonDetailComponent,
    resolve: {
      salesperson: SalespersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.salesperson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SalespersonUpdateComponent,
    resolve: {
      salesperson: SalespersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.salesperson.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SalespersonUpdateComponent,
    resolve: {
      salesperson: SalespersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.salesperson.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const salespersonPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SalespersonDeletePopupComponent,
    resolve: {
      salesperson: SalespersonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.salesperson.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
