import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { ISalesperson } from 'app/shared/model/salesperson.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SalespersonService } from './salesperson.service';
import { SalespersonCriteria } from 'app/entities/salesperson/salesperson-criteria';

@Component({
  selector: 'jhi-salesperson',
  templateUrl: './salesperson.component.html'
})
export class SalespersonComponent implements OnInit, OnDestroy {
  currentAccount: any;
  salespeople: ISalesperson[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  salespersonCriteria: SalespersonCriteria = new SalespersonCriteria();

  constructor(
    protected salespersonService: SalespersonService,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.salespersonService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ISalesperson[]>) => this.paginateSalespeople(res.body, res.headers));
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/salesperson'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.findSalespeople(this.page);
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/salesperson',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSalespeople();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISalesperson) {
    return item.id;
  }

  registerChangeInSalespeople() {
    this.eventSubscriber = this.eventManager.subscribe('salespersonListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSalespeople(data: ISalesperson[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.salespeople = data;
  }

  findSalespeople(pageNum: number) {
    this.salespersonService
      .findByCriteria(
        {
          page: pageNum - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        },
        this.salespersonCriteria
      )
      .subscribe((res: HttpResponse<ISalesperson[]>) => {
        // eslint-disable-next-line no-console
        this.paginateSalespeople(res.body, res.headers);
      });
  }
}
