import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrmSharedModule } from 'app/shared/shared.module';
import { SalespersonComponent } from './salesperson.component';
import { SalespersonDetailComponent } from './salesperson-detail.component';
import { SalespersonUpdateComponent } from './salesperson-update.component';
import { SalespersonDeletePopupComponent, SalespersonDeleteDialogComponent } from './salesperson-delete-dialog.component';
import { salespersonRoute, salespersonPopupRoute } from './salesperson.route';
import { SalespersonFilterComponent } from 'app/entities/salesperson/salesperson-filter.component';

const ENTITY_STATES = [...salespersonRoute, ...salespersonPopupRoute];

@NgModule({
  imports: [CrmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SalespersonComponent,
    SalespersonDetailComponent,
    SalespersonUpdateComponent,
    SalespersonDeleteDialogComponent,
    SalespersonDeletePopupComponent,
    SalespersonFilterComponent
  ],
  entryComponents: [SalespersonDeleteDialogComponent]
})
export class CrmSalespersonModule {}
