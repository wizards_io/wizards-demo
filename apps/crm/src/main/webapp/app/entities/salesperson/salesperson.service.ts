import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISalesperson } from 'app/shared/model/salesperson.model';

type EntityResponseType = HttpResponse<ISalesperson>;
type EntityArrayResponseType = HttpResponse<ISalesperson[]>;

@Injectable({ providedIn: 'root' })
export class SalespersonService {
  public resourceUrl = SERVER_API_URL + 'api/salespeople';
  public resourceUrlFilter = SERVER_API_URL + 'api/salespeople/find';

  constructor(protected http: HttpClient) {}

  create(salesperson: ISalesperson): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(salesperson);
    return this.http
      .post<ISalesperson>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(salesperson: ISalesperson): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(salesperson);
    return this.http
      .put<ISalesperson>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISalesperson>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISalesperson[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(salesperson: ISalesperson): ISalesperson {
    const copy: ISalesperson = Object.assign({}, salesperson, {
      birthDate: salesperson.birthDate != null && salesperson.birthDate.isValid() ? salesperson.birthDate.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.birthDate = res.body.birthDate != null ? moment(res.body.birthDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((salesperson: ISalesperson) => {
        salesperson.birthDate = salesperson.birthDate != null ? moment(salesperson.birthDate) : null;
      });
    }
    return res;
  }

  findByCriteria(req?: any, criteria?: any) {
    const options = createRequestOption(req);
    return this.http
      .post<ISalesperson[]>(this.resourceUrlFilter, criteria, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
}
