import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISalesperson } from 'app/shared/model/salesperson.model';

@Component({
  selector: 'jhi-salesperson-detail',
  templateUrl: './salesperson-detail.component.html'
})
export class SalespersonDetailComponent implements OnInit {
  salesperson: ISalesperson;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ salesperson }) => {
      this.salesperson = salesperson;
    });
  }

  previousState() {
    window.history.back();
  }
}
