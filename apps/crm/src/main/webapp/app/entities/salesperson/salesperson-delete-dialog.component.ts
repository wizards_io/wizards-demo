import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISalesperson } from 'app/shared/model/salesperson.model';
import { SalespersonService } from './salesperson.service';

@Component({
  selector: 'jhi-salesperson-delete-dialog',
  templateUrl: './salesperson-delete-dialog.component.html'
})
export class SalespersonDeleteDialogComponent {
  salesperson: ISalesperson;

  constructor(
    protected salespersonService: SalespersonService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.salespersonService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'salespersonListModification',
        content: 'Deleted an salesperson'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-salesperson-delete-popup',
  template: ''
})
export class SalespersonDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ salesperson }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SalespersonDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.salesperson = salesperson;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/salesperson', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/salesperson', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
