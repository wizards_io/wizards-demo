export class CustomerCriteria {
  id: string;
  name: string;
  nip: string;
  regon: string;
}
