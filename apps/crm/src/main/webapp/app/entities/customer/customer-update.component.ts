import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICustomer, Customer } from 'app/shared/model/customer.model';
import { CustomerService } from './customer.service';

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html'
})
export class CustomerUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    website: [],
    email: [],
    nip: [],
    regon: [],
    phoneNumber: [null, [Validators.maxLength(20)]],
    streetName: [],
    zipCode: [],
    city: [],
    buildingNumber: [],
    localNumber: [],
    createDate: [],
    updateDate: []
  });

  constructor(protected customerService: CustomerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ customer }) => {
      this.updateForm(customer);
    });
  }

  updateForm(customer: ICustomer) {
    this.editForm.patchValue({
      id: customer.id,
      name: customer.name,
      website: customer.website,
      email: customer.email,
      nip: customer.nip,
      regon: customer.regon,
      phoneNumber: customer.phoneNumber,
      streetName: customer.streetName,
      zipCode: customer.zipCode,
      city: customer.city,
      buildingNumber: customer.buildingNumber,
      localNumber: customer.localNumber,
      createDate: customer.createDate,
      updateDate: customer.updateDate
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const customer = this.createFromForm();
    if (customer.id !== undefined) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  private createFromForm(): ICustomer {
    return {
      ...new Customer(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      website: this.editForm.get(['website']).value,
      email: this.editForm.get(['email']).value,
      nip: this.editForm.get(['nip']).value,
      regon: this.editForm.get(['regon']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      streetName: this.editForm.get(['streetName']).value,
      zipCode: this.editForm.get(['zipCode']).value,
      city: this.editForm.get(['city']).value,
      buildingNumber: this.editForm.get(['buildingNumber']).value,
      localNumber: this.editForm.get(['localNumber']).value,
      createDate: this.editForm.get(['createDate']).value,
      updateDate: this.editForm.get(['updateDate']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
