import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CustomerCriteria } from 'app/entities/customer/customer-criteria';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

@Component({
  selector: 'jhi-customer-filter',
  templateUrl: './customer-filter.component.html'
})
export class CustomerFilterComponent {
  @Input() request: CustomerCriteria;
  @Output() onFind: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  find() {
    this.onFind.emit(this.request);
  }

  clear() {
    this.request.id = null;
    this.request.name = null;
    this.request.nip = null;
    this.request.regon = null;
    this.onFind.emit();
  }
}
