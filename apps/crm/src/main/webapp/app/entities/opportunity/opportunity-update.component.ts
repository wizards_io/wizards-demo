import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IOpportunity, Opportunity } from 'app/shared/model/opportunity.model';
import { OpportunityService } from './opportunity.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';
import { ISalesperson } from 'app/shared/model/salesperson.model';
import { SalespersonService } from 'app/entities/salesperson/salesperson.service';
import { IContact } from 'app/shared/model/contact.model';
import { ContactService } from 'app/entities/contact/contact.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { opportunityRoute } from 'app/entities/opportunity/opportunity.route';

@Component({
  selector: 'jhi-opportunity-update',
  templateUrl: './opportunity-update.component.html'
})
export class OpportunityUpdateComponent implements OnInit {
  isSaving: boolean;

  customers: ICustomer[];

  salespeople: ISalesperson[];

  contacts: IContact[];

  products: IProduct[];
  createdDp: any;
  closeDateDp: any;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    stage: [],
    probability: [null, [Validators.min(0), Validators.max(100)]],
    description: [],
    created: [],
    closeDate: [],
    customerId: [],
    salespeople: [],
    contacts: [],
    products: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected opportunityService: OpportunityService,
    protected customerService: CustomerService,
    protected salespersonService: SalespersonService,
    protected contactService: ContactService,
    protected productService: ProductService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ opportunity }) => {
      if (opportunity.customerId !== null) {
        this.findContactsByCustomerId(opportunity.customerId);
      }
      this.updateForm(opportunity);
    });
    this.customerService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICustomer[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICustomer[]>) => response.body)
      )
      .subscribe((res: ICustomer[]) => (this.customers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.salespersonService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISalesperson[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISalesperson[]>) => response.body)
      )
      .subscribe((res: ISalesperson[]) => (this.salespeople = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduct[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduct[]>) => response.body)
      )
      .subscribe((res: IProduct[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(opportunity: IOpportunity) {
    this.editForm.patchValue({
      id: opportunity.id,
      name: opportunity.name,
      stage: opportunity.stage,
      probability: opportunity.probability,
      description: opportunity.description,
      created: opportunity.created,
      closeDate: opportunity.closeDate,
      customerId: opportunity.customerId,
      salespeople: opportunity.salespeople,
      // contacts: opportunity.contacts,
      products: opportunity.products
    });
  }

  updateContact(contacts: IContact[]) {
    this.editForm.patchValue({
      contacts
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const opportunity = this.createFromForm();
    if (opportunity.id !== undefined) {
      this.subscribeToSaveResponse(this.opportunityService.update(opportunity));
    } else {
      this.subscribeToSaveResponse(this.opportunityService.create(opportunity));
    }
  }

  private createFromForm(): IOpportunity {
    return {
      ...new Opportunity(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      stage: this.editForm.get(['stage']).value,
      probability: this.editForm.get(['probability']).value,
      description: this.editForm.get(['description']).value,
      created: this.editForm.get(['created']).value,
      closeDate: this.editForm.get(['closeDate']).value,
      customerId: this.editForm.get(['customerId']).value,
      salespeople: this.editForm.get(['salespeople']).value,
      contacts: this.editForm.get(['contacts']).value,
      products: this.editForm.get(['products']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOpportunity>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCustomerById(index: number, item: ICustomer) {
    return item.id;
  }

  trackSalespersonById(index: number, item: ISalesperson) {
    return item.id;
  }

  trackContactById(index: number, item: IContact) {
    return item.id;
  }

  trackProductById(index: number, item: IProduct) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  onCustomerSelect(value: string) {
    const customerId = value.split(': ')[1];
    if (customerId !== null) {
      this.findContactsByCustomerId(Number(customerId));
    }
  }

  private findContactsByCustomerId(cusId: number) {
    this.contactService
      .query({ customerId: cusId })
      .pipe(
        filter((mayBeOk: HttpResponse<IContact[]>) => mayBeOk.ok),
        map((response: HttpResponse<IContact[]>) => response.body)
      )
      .subscribe(
        (res: IContact[]) => {
          this.contacts = res.filter(e => e.customerId === cusId);
          this.updateContact(this.contacts);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }
}
