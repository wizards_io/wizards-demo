import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Opportunity } from 'app/shared/model/opportunity.model';
import { OpportunityService } from './opportunity.service';
import { OpportunityComponent } from './opportunity.component';
import { OpportunityDetailComponent } from './opportunity-detail.component';
import { OpportunityUpdateComponent } from './opportunity-update.component';
import { OpportunityDeletePopupComponent } from './opportunity-delete-dialog.component';
import { IOpportunity } from 'app/shared/model/opportunity.model';

@Injectable({ providedIn: 'root' })
export class OpportunityResolve implements Resolve<IOpportunity> {
  constructor(private service: OpportunityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOpportunity> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Opportunity>) => response.ok),
        map((opportunity: HttpResponse<Opportunity>) => opportunity.body)
      );
    }
    return of(new Opportunity());
  }
}

export const opportunityRoute: Routes = [
  {
    path: '',
    component: OpportunityComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'crmApp.opportunity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OpportunityDetailComponent,
    resolve: {
      opportunity: OpportunityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.opportunity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OpportunityUpdateComponent,
    resolve: {
      opportunity: OpportunityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.opportunity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OpportunityUpdateComponent,
    resolve: {
      opportunity: OpportunityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.opportunity.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const opportunityPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OpportunityDeletePopupComponent,
    resolve: {
      opportunity: OpportunityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crmApp.opportunity.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
