import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrmSharedModule } from 'app/shared/shared.module';
import { OpportunityComponent } from './opportunity.component';
import { OpportunityDetailComponent } from './opportunity-detail.component';
import { OpportunityUpdateComponent } from './opportunity-update.component';
import { OpportunityDeletePopupComponent, OpportunityDeleteDialogComponent } from './opportunity-delete-dialog.component';
import { opportunityRoute, opportunityPopupRoute } from './opportunity.route';

const ENTITY_STATES = [...opportunityRoute, ...opportunityPopupRoute];

@NgModule({
  imports: [CrmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OpportunityComponent,
    OpportunityDetailComponent,
    OpportunityUpdateComponent,
    OpportunityDeleteDialogComponent,
    OpportunityDeletePopupComponent
  ],
  entryComponents: [OpportunityDeleteDialogComponent]
})
export class CrmOpportunityModule {}
