import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'salesperson',
        loadChildren: () => import('./salesperson/salesperson.module').then(m => m.CrmSalespersonModule)
      },
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.CrmCustomerModule)
      },
      {
        path: 'contact',
        loadChildren: () => import('./contact/contact.module').then(m => m.CrmContactModule)
      },
      {
        path: 'opportunity',
        loadChildren: () => import('./opportunity/opportunity.module').then(m => m.CrmOpportunityModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.CrmProductModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class CrmEntityModule {}
