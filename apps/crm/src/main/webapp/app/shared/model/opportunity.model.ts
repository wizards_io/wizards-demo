import { Moment } from 'moment';
import { ISalesperson } from 'app/shared/model/salesperson.model';
import { IContact } from 'app/shared/model/contact.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IOpportunity {
  id?: number;
  name?: string;
  stage?: string;
  probability?: number;
  description?: string;
  created?: Moment;
  closeDate?: Moment;
  customerName?: string;
  customerId?: number;
  salespeople?: ISalesperson[];
  contacts?: IContact[];
  products?: IProduct[];
}

export class Opportunity implements IOpportunity {
  constructor(
    public id?: number,
    public name?: string,
    public stage?: string,
    public probability?: number,
    public description?: string,
    public created?: Moment,
    public closeDate?: Moment,
    public customerName?: string,
    public customerId?: number,
    public salespeople?: ISalesperson[],
    public contacts?: IContact[],
    public products?: IProduct[]
  ) {}
}
