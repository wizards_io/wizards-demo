import { IContact } from 'app/shared/model/contact.model';
import { IOpportunity } from 'app/shared/model/opportunity.model';
import { Moment } from 'moment';
import { IPurchase } from 'app/shared/model/purchase.model';

export interface ICustomer {
  id?: number;
  name?: string;
  website?: string;
  email?: string;
  phoneNumber?: string;
  streetName?: string;
  zipCode?: string;
  city?: string;
  buildingNumber?: string;
  localNumber?: string;
  nip?: string;
  regon?: string;
  contacts?: IContact[];
  opportunities?: IOpportunity[];
  purchases?: IPurchase[];
  createDate?: Moment;
  updateDate?: Moment;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public name?: string,
    public website?: string,
    public email?: string,
    public phoneNumber?: string,
    public streetName?: string,
    public zipCode?: string,
    public city?: string,
    public buildingNumber?: string,
    public localNumber?: string,
    public nip?: string,
    public regon?: string,
    public contacts?: IContact[],
    public opportunities?: IOpportunity[],
    public purchases?: IPurchase[],
    public createDate?: Moment,
    public updateDate?: Moment
  ) {}
}
