import { Moment } from 'moment';
import { IProduct } from 'app/shared/model/product.model';

export interface IPurchase {
  id?: number;
  product?: IProduct;
  createDate?: Moment;
  finishDate?: Moment;
}

export class Purchase implements IPurchase {
  constructor(public id?: number, public product?: IProduct, public createDate?: Moment, public finishDate?: Moment) {}
}
