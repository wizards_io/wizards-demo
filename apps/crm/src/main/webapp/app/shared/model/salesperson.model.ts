import { Moment } from 'moment';
import { IOpportunity } from 'app/shared/model/opportunity.model';

export interface ISalesperson {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  pesel?: string;
  birthDate?: Moment;
  phoneNumber?: string;
  salary?: number;
  jobTitle?: string;
  streetName?: string;
  zipCode?: string;
  city?: string;
  buildingNumber?: string;
  localNumber?: string;
  opportunities?: IOpportunity[];
  createDate?: Moment;
  updateDate?: Moment;
}

export class Salesperson implements ISalesperson {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public pesel?: string,
    public birthDate?: Moment,
    public phoneNumber?: string,
    public salary?: number,
    public jobTitle?: string,
    public streetName?: string,
    public zipCode?: string,
    public city?: string,
    public buildingNumber?: string,
    public localNumber?: string,
    public opportunities?: IOpportunity[],
    public createDate?: Moment,
    public updateDate?: Moment
  ) {}
}
