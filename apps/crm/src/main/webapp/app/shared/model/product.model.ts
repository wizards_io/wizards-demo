import { IOpportunity } from 'app/shared/model/opportunity.model';

export interface IProduct {
  id?: number;
  name?: string;
  category?: string;
  code?: string;
  opportunities?: IOpportunity[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public category?: string,
    public code?: string,
    public opportunities?: IOpportunity[]
  ) {}
}
