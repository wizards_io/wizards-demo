import { IOpportunity } from 'app/shared/model/opportunity.model';
import { Moment } from 'moment';

export interface IContact {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  jobTitle?: string;
  streetName?: string;
  zipCode?: string;
  city?: string;
  buildingNumber?: string;
  localNumber?: string;
  customerName?: string;
  customerId?: number;
  opportunities?: IOpportunity[];
  createDate?: Moment;
  updateDate?: Moment;
}

export class Contact implements IContact {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phoneNumber?: string,
    public streetName?: string,
    public zipCode?: string,
    public city?: string,
    public buildingNumber?: string,
    public localNumber?: string,
    public customerName?: string,
    public customerId?: number,
    public opportunities?: IOpportunity[],
    public createDate?: Moment,
    public updateDate?: Moment
  ) {}
}
