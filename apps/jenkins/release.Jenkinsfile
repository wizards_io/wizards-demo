#!/usr/bin/env groovy
pipeline {
    agent {
        label 'slave-pr-09'
    }
    environment {
        DOCKER_REGISTRY_HARBOR = 'harbor.isolution.pl'
        DOCKER_REPOSITORY_HARBOR = 'wizards'
        DOCKER_REPOSITORY_DOCKERHUB = 'wizardsio'
        DOCKER_IMAGE_CRM = 'wizards-demo-crm'
        DOCKER_IMAGE_HR = 'wizards-demo-hr'
        ROOT_DIR = 'apps'
        TAG = null
        SEMVER = null
        SEMVER_MAJOR = null
        SEMVER_MINOR = null
        SEMVER_PATCH = null
    }
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3-jdk-13'
                    args '-v $HOME/m2repositories/wizards/wizards-demo-release/.m2:/var/maven/.m2 -e MAVEN_CONFIG=/var/maven/.m2 -e HOME=.'
                }
            }
            steps {
                dir("${ROOT_DIR}/crm") {
                    sh "mvn clean package --batch-mode -DskipTests -Duser.home=/var/maven"
                }
                dir("${ROOT_DIR}/hr") {
                    sh "mvn clean package --batch-mode -DskipTests -Duser.home=/var/maven"
                }
                stash includes: "${ROOT_DIR}/crm/target/*.jar", name: 'crmJar'
                stash includes: "${ROOT_DIR}/hr/target/*.jar", name: 'hrJar'
            }
        }

        stage('Release (Docker)') {
            agent {
                docker {
                    image "${DOCKER_REGISTRY_HARBOR}/${DOCKER_REPOSITORY_HARBOR}/buildenv-docker:19.03-0"
                    args '-v /var/run/docker.sock:/var/run/docker.sock -u :993'
                }
            }
            steps {
                unstash 'crmJar'
                unstash 'hrJar'
                dir("${ROOT_DIR}/crm") {
                    sh "docker build -t ${DOCKER_REPOSITORY_DOCKERHUB}/${DOCKER_IMAGE_CRM}:latest ."
                }
                dir("${ROOT_DIR}/hr") {
                    sh "docker build -t ${DOCKER_REPOSITORY_DOCKERHUB}/${DOCKER_IMAGE_HR}:latest ."
                }
                withDockerRegistry([credentialsId: 'wizards-docker-hub', url: null]) {
                    sh "docker push ${DOCKER_REPOSITORY_DOCKERHUB}/${DOCKER_IMAGE_CRM}:latest"
                    sh "docker push ${DOCKER_REPOSITORY_DOCKERHUB}/${DOCKER_IMAGE_HR}:latest"
                }
            }
        }
    }
}