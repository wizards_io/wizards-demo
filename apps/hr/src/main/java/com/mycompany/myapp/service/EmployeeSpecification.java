package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Employee;
import com.mycompany.myapp.domain.Employee_;
import com.mycompany.myapp.service.dto.EmployeeFindRequest;
import io.github.jhipster.service.QueryService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;

@Service
public class EmployeeSpecification extends QueryService<Employee> {

    public Specification<Employee> createSpecification(EmployeeFindRequest criteria) {
        return (root, cq, cb) -> {
            Predicate pr = cb.isNotNull(root.get(Employee_.FIRST_NAME));

            if (criteria.getId() != null) {
                try {
                    Long requestedId = Long.parseLong(criteria.getId());
                    pr = cb.and(pr, cb.equal(root.get(Employee_.ID), requestedId));
                } catch (NumberFormatException e) {
                    pr = cb.isTrue(cb.isNull(root.get(Employee_.ID)));
                }
            }
            if (criteria.getFirstName() != null && !criteria.getFirstName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Employee_.FIRST_NAME)), criteria.getFirstName().toLowerCase()));
            }
            if (criteria.getLastName() != null && !criteria.getLastName().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Employee_.LAST_NAME)), criteria.getLastName().toLowerCase()));
            }
            if (criteria.getEmail() != null && !criteria.getEmail().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Employee_.EMAIL)), criteria.getEmail().toLowerCase()));
            }
            if (criteria.getPesel() != null && !criteria.getPesel().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Employee_.PESEL)), criteria.getPesel().toLowerCase()));
            }
            if (criteria.getJobTitle() != null && !criteria.getJobTitle().isEmpty()) {
                pr = cb.and(pr, cb.equal(cb.lower(root.get(Employee_.JOB_TITLE)), criteria.getJobTitle().toLowerCase()));
            }
            return pr;
        };
    }

//        Specification<Employee> specification = Specification.where(null);
//        if (criteria != null) {
//            if (criteria.getFirstName() != null) {
//                specification = specification.and(buildStringSpecification(criteria.getFirstName(), Employee_.firstName));
//
//            }
//        }
//        return specification;
    }
