export class EmployeeCriteria {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  pesel: string;
  jobTitle: string;
}
