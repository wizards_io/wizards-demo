import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EmployeeSharedModule } from 'app/shared/shared.module';
import { EmployeeComponent } from './employee.component';
import { EmployeeDetailComponent } from './employee-detail.component';
import { EmployeeUpdateComponent } from './employee-update.component';
import { EmployeeDeleteDialogComponent, EmployeeDeletePopupComponent } from './employee-delete-dialog.component';
import { employeePopupRoute, employeeRoute } from './employee.route';
import { MatExpansionModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { EmployeeFilterComponent } from 'app/entities/employee/employee-filter.component';

const ENTITY_STATES = [...employeeRoute, ...employeePopupRoute];

@NgModule({
  imports: [EmployeeSharedModule, RouterModule.forChild(ENTITY_STATES), MatExpansionModule, CommonModule],
  declarations: [
    EmployeeComponent,
    EmployeeDetailComponent,
    EmployeeUpdateComponent,
    EmployeeDeleteDialogComponent,
    EmployeeDeletePopupComponent,
    EmployeeFilterComponent
  ],
  entryComponents: [EmployeeDeleteDialogComponent]
})
export class EmployeeEmployeeModule {}
