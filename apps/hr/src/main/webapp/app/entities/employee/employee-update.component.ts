import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IEmployee, Employee } from 'app/shared/model/employee.model';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'jhi-employee-update',
  templateUrl: './employee-update.component.html'
})
export class EmployeeUpdateComponent implements OnInit {
  isSaving: boolean;
  birthDateDp: any;

  editForm = this.fb.group({
    id: [],
    firstName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    lastName: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    email: [],
    pesel: [null, [Validators.minLength(11), Validators.maxLength(11)]],
    birthDate: [],
    phoneNumber: [null, [Validators.maxLength(20)]],
    salary: [null, [Validators.min(0)]],
    jobTitle: [],
    streetName: [],
    zipCode: [],
    city: [],
    buildingNumber: [],
    localNumber: [],
    createDate: [],
    updateDate: []
  });

  constructor(protected employeeService: EmployeeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ employee }) => {
      this.updateForm(employee);
    });
  }

  updateForm(employee: IEmployee) {
    this.editForm.patchValue({
      id: employee.id,
      firstName: employee.firstName,
      lastName: employee.lastName,
      email: employee.email,
      pesel: employee.pesel,
      birthDate: employee.birthDate,
      phoneNumber: employee.phoneNumber,
      salary: employee.salary,
      jobTitle: employee.jobTitle,
      streetName: employee.streetName,
      zipCode: employee.zipCode,
      city: employee.city,
      buildingNumber: employee.buildingNumber,
      localNumber: employee.localNumber,
      createDate: employee.createDate,
      updateDate: employee.updateDate
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const employee = this.createFromForm();
    if (employee.id !== undefined) {
      this.subscribeToSaveResponse(this.employeeService.update(employee));
    } else {
      this.subscribeToSaveResponse(this.employeeService.create(employee));
    }
  }

  private createFromForm(): IEmployee {
    return {
      ...new Employee(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      email: this.editForm.get(['email']).value,
      pesel: this.editForm.get(['pesel']).value,
      birthDate: this.editForm.get(['birthDate']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      salary: this.editForm.get(['salary']).value,
      jobTitle: this.editForm.get(['jobTitle']).value,
      streetName: this.editForm.get(['streetName']).value,
      zipCode: this.editForm.get(['zipCode']).value,
      city: this.editForm.get(['city']).value,
      buildingNumber: this.editForm.get(['buildingNumber']).value,
      localNumber: this.editForm.get(['localNumber']).value,
      createDate: this.editForm.get(['createDate']).value,
      updateDate: this.editForm.get(['updateDate']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
