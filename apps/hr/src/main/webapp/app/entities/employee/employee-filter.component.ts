import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EmployeeCriteria } from 'app/entities/employee/employee-criteria';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

@Component({
  selector: 'jhi-employee-filter',
  templateUrl: './employee-filter.component.html'
})
export class EmployeeFilterComponent {
  @Input() request: EmployeeCriteria;
  @Output() onFind: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  find() {
    this.onFind.emit(this.request);
  }

  clear() {
    this.request.id = null;
    this.request.firstName = null;
    this.request.lastName = null;
    this.request.email = null;
    this.request.pesel = null;
    this.request.jobTitle = null;
    this.onFind.emit();
  }
}
